// $Id$

The EthifyGame module provides some basic functionality for level based games.
This module does not ship any levels to play with.

Functionality:
 + enable (register) / disable levels
 + mark / unmark levels as solved
 + check access to levels
 + fetching level-information
   - first / last
   - previous / next
   - by id / level / name
   - highest playable
 + redirect to specific level
 + set level sequence

Requirements
------------
 + Drupal 6
 + Modules
   - path
 
Installation
------------
1) Copy the EthifyGame directory to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)

Configuration
-------------

1) Configure the Permission for the game
   Administer -> User Management -> Permissions (/admin/user/permissions)

2) Configure default settings for the game
   Administer -> Site Configuration -> Ethifygame (/admin/settings/ethifygame)

Using the module
----------------

Enable a level:
  // enabling the level in the hook_enable() function.
  function game_example_enable() {
    // enable the leve with the parameters:
    // 'GameExample' as name (unique name of the level)
    // 'ethifygame/level/game_example' as url - the user gets redirected to
    //   this url, when he play that level. The url will be redirected with
    //   the core drupal function drupal_goto(url);
    ethifygame_level_enable('GameExample', 'ethifygame/level/game_example');
  }
  
Disable a level:
  // diable the level in the hook_disable() function.
  function game_example_disable() {
    // disable the level by passing the level-name as argument.
    ethifygame_level_disable('GameExample');
  }
  
Check game access:
  // to check, whether the user is allowed to play a game, you can
  // use the user_access() method with the condition 'play ethifygame':
  $access = user_access('play ethifygame');
  if ($access) {
    // the user can play games. Code here...
  }
  else {
    // the user is not allowed to play games.
    drupal_access_denied();
    drupal_exit();
  }

Check level access:
  // load the level 'GameExample'
  $level = ethifygame::factory_get_level_by_name('GameExample');
  // You may check, if the $level object is valid with $level->is_entity().
  if (!$level->is_entity()) {
    // The level 'GameExample' does not exist!
    // If you do not check, if the $level object is valid, the has_access()
    // method returns FALSE.
    // Your Code here...
  }
  else {
    // Check, whether the current user has access to the level,
    // stored in $level
    $access = $level->has_access();
    if ($access) {
      // User has access to play this level. Your code here...
    }
    else {
      // The user needs to play some other levels, to get access to play
      // this level.
      // Set an error message for the user.
      drupal_set_message(t('Play some levels to gain access for level' .
          ' GameExample.'), 'error');
      // redirect the user to a valid level, to play the game.
      ethifygame_goto();
    }
  }