<?php
// $Id$

/**
 * @file
 * Implements the game logic of the EthifyGame.
 */


define('ETHIFYGAME_LEVEL_NR_FIRST', 1);
define('ETHIFYGAME_LEVEL_STATE_UNKNOWN', NULL);
define('ETHIFYGAME_LEVEL_STATE_INACTIVE', 0);
define('ETHIFYGAME_LEVEL_STATE_ACTIVE', 1);


/**
 * Enables a EthifyGame Level with the given parameters. The users are now able to play this level.
 * @param string $name
 *     The unique name of the game level
 * @param string $start_path
 *     The start path of this level (url)
 */
function ethifygame_level_enable($name, $start_path) {
  $o = ethifygame::factory_get_level_by_name($name);
  if( !$o->is_entity() ) {
    $o->get_entity_ref()->set_name($name);
  }
  $o->get_entity_ref()->set_start_path($start_path);
  $o->get_entity_ref()->set_state(ETHIFYGAME_LEVEL_STATE_ACTIVE);
  $o->save();
  return $o;
}
/**
 * Disables a EthifyGame Level by the given name. Users cannot play this level, until it gets enabled again.
 * @param string $name
 *     The name of the level
 */
function ethifygame_level_disable($name) {
  $level_o = ethifygame::factory_get_level_by_name($name);
  if( $level_o->is_entity() ) {
    $level_o->get_entity_ref()->set_state(ETHIFYGAME_LEVEL_STATE_INACTIVE);
    $level_o->save();
  }
  return $o;
}

/**
 * @return
 *     Returns a list of available levels
 */
function ethifygame_get_levels() {
  $levels = array();
  
  $result = db_query(
      'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
      ' FROM {ethifygame_levels}' .
      ' ORDER BY eg_level_nr ASC, eg_level_state DESC'
  );
  
  while( $level = db_fetch_array($result) ) {
    $levels[] = new ethifygame_level($level);
  }
  
  return $levels;
}

class ethifygame {
  
  private static $first_level = NULL;
  private static $last_level = NULL;
  
  private $entity = NULL; // type = ethifygame_level
  
  /**
   * Saves the current instance
   * 
   * @return
   *     Returns, whether the save operation was done correct or not.
   */
  public function save() {
    if( !$this->is_valid() ) {
      return false;
    }
    
    // TODO: check, if db_* was successfull!
    // first, search the entity in the database
    $db_entity = NULL;
    if( $this->entity->get_id() === NULL ) {
      $db_entity = new ethifygame_level(
        db_fetch_array(
          db_query(
            'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
            ' FROM {ethifygame_levels}' .
            " WHERE eg_level_name = '%s'",
            $this->entity->get_name()
      )));
    }
    else {
      $db_entity = new ethifygame_level(
        db_fetch_array(
          db_query(
            'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
            ' FROM {ethifygame_levels}' .
            ' WHERE eg_level_id = %d',
            $this->entity->get_id()
      )));
    }
    $is_new = $db_entity->get_id() === NULL;
    if( $is_new && $this->entity->get_id() !== NULL ) {
      // there does not exist an entity with the given id!
      return false;
    }
    
    // get the maximum level nr from the database
    $max_level_nr = db_result(db_query('SELECT MAX(eg_level_nr) FROM {ethifygame_levels} WHERE eg_level_state = %d', ETHIFYGAME_LEVEL_STATE_ACTIVE));
    if( $max_level_nr === FALSE ) {
      $max_level_nr = 0;
    }
    
    
    // set level id, if it does not exist or is not valid!
    if( $is_new ) {
      if( $this->entity->get_level() === NULL ||
          $this->entity->get_level() > $max_level_nr + 1) {
        $this->entity->set_level($max_level_nr + 1);
      }
      else if( $this->entity->get_level() < ETHIFYGAME_LEVEL_NR_FIRST ) {
        $this->entity->set_level(ETHIFYGAME_LEVEL_NR_FIRST);
      }
    }
    else {
      if( $this->entity->get_level() === NULL ||
          $this->entity->get_level() > $max_level_nr ) {
        if( $db_entity->get_state() == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
          $this->entity->set_level($max_level_nr);
        }
        else {
          $this->entity->set_level($max_level_nr + 1);
        }
      }
      else if ( $this->entity->get_level() < ETHIFYGAME_LEVEL_NR_FIRST ) {
        $this->entity->set_level(ETHIFYGAME_LEVEL_NR_FIRST);
      }
    }
    
    // move existing entires, to insert the level
    if( $this->entity->get_state() == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
      if( $is_new ) {
        db_query('UPDATE {ethifygame_levels} SET eg_level_nr = eg_level_nr + 1 WHERE eg_level_nr >= %d', $this->entity->get_level());
      }
      else {
        $diff_level = $db_entity->get_level() - $this->entity->get_level();
        if($db_entity->get_state() == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
          // same state, just move the levels
          if( $diff_level > 0 ) {
            // level moves to beginning
            db_query('UPDATE {ethifygame_levels} SET eg_level_nr = eg_level_nr + 1 WHERE eg_level_nr >= %d AND eg_level_nr < %d', $this->entity->get_level(), $db_entity->get_level());
          }
          else if( $diff_level < 0 ) {
            // level moves to end
            db_query('UPDATE {ethifygame_levels} SET eg_level_nr = eg_level_nr - 1 WHERE eg_level_nr > %d AND eg_level_nr <= %d', $db_entity->get_level(), $this->entity->get_level());
          } // else level does not move. no change
        }
        else {
          // not the same state - insert
          db_query('UPDATE {ethifygame_levels} SET eg_level_nr = eg_level_nr + 1 WHERE eg_level_nr >= %d', $this->entity->get_level());
        }
      }
    }
    else {
      if( !$is_new ) {
        $diff_level = $db_entity->get_level() - $this->entity->get_level();
        if($db_entity->get_state() == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
          // not the same state, move the levels
          db_query('UPDATE {ethifygame_levels} SET eg_level_nr = eg_level_nr - 1 WHERE eg_level_nr > %d', $db_entity->get_level());
        } // else, we just move the current object
      } // else, if it is new, we do not need to do anything!
    }
    
    // lets update the current level itself
    if( !$is_new ) {
      // update
      db_query(
        'UPDATE {ethifygame_levels}'.
        " SET eg_level_name = '%s', eg_level_entry_path = '%s', eg_level_nr = %d, eg_level_state = %d" .
        ' WHERE eg_level_id = %d',
        $this->entity->get_name(),
        $this->entity->get_start_path(),
        $this->entity->get_level(),
        $this->entity->get_state(),
        $this->entity->get_id()
      );
    }
    else {
      // insert
      db_query(
        'INSERT INTO {ethifygame_levels}' .
        ' (eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state)' .
        " VALUES ('%s', '%s', %d, %d)",
        $this->entity->get_name(),
        $this->entity->get_start_path(),
        $this->entity->get_level(),
        $this->entity->get_state()
      );
      
      $this->entity->set_id(db_last_insert_id('ethifygame_levels', 'eg_level_id'));
    }
    
    if( $max_level_nr <= $this->entity->get_level() && $this->entity->get_state() == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
      self::$last_level = NULL;
    }
    if( $this->entity->get_level() == ETHIFYGAME_LEVEL_NR_FIRST ) {
      self::$first_level = NULL;
    }
    
    return true;
  }
  
  public function is_valid() {
    if( $this->entity === NULL ) {
      return false;
    }
    
    $entity = $this->get_entity();
    
    if( $entity->get_name() === NULL || !strlen($entity->get_name()) ) {
      // name is not set
      return fales;
    }
    
    if( $entity->get_start_path() === NULL || !strlen($entity->get_start_path()) ) {
      // start path is not set
      return false;
    }
    
    if( $entity->get_state() === ETHIFYGAME_LEVEL_STATE_UNKNOWN ) {
      // state is invalid
      return false;
    }
    
    return true;
  }
  
  /**
   * Checks, whether the user has played this level or not.
   * 
   * @param mixed $level
   *     The level (nr | name | NULL = current object)
   * @return
   *     Returns TRUE, if the level is already played by the current user, otherwise FALSE.
   */
  public function is_level_played($level = NULL) {
    $level_current = $this->get_level($level);
    if( !$level_current->is_entity() ) {
      return FALSE;
    }
    
    $entries = $this->get_user_level_entries($level_current->get_entity()->get_id());
    return count($entries) > 0;
  }
  /**
   * Marks a level, whether it is sucessfully played or not
   * 
   * @param mixed $level
   *     The level (nr | name | NULL = current object)
   * @param boolean $level_done
   *     TRUE will allow the user to go to the next level
   *     FALSE the user can not play the next level
   */
  public function mark_level($level = NULL, $level_done = TRUE) {
    $level_current = $this->get_level($level);
    if( !$level_current->is_entity() ) {
      return FALSE;
    }
    
    $entries = $this->get_user_level_entries($level_current->get_entity()->get_id());
    if( count($entities) > 0 ) {
      if( !$level_done ) {
        // there are entries. we will unmark these entries
        $this->unmark_user_level_entries($level_current->get_entity()->get_id());
      }
    }
    else if( $level_done ) {
      // no entries are available. mark the current user
      $this->mark_user_level_entry($level_current->get_entity()->get_id());
    }
    
    return TRUE;
  }
  
  /**
   * Removes the database entries of the current user and the current level_id.
   * 
   * @param int $level_id
   *     The level_id or NULL for all levels
   */
  private function unmark_user_level_entries($level_id) {
    global $user;
    
    if( $level_id == NULL ) {
      db_query('DELETE FROM {ethifygame_played} WHERE eg_played_user_id = %d', $user->uid);
    }
    else {
      db_query(
        'DELETE' .
        ' FROM {ethifygame_played}' .
        ' WHERE eg_played_user_id = %d AND eg_played_level_id = %d',
        $user->uid, $level_id
      );
    }
  }
  
  /**
   * Creates a database entry for the given level and the current user.
   * 
   * @param int $level_id
   *     The level_id
   */
  private function mark_user_level_entry($level_id) {
    global $user;
    db_query(
      'INSERT INTO {ethifygame_played}' .
      ' (eg_played_user_id, eg_played_level_id, eg_played_activate_timestamp)' .
      ' VALUES (%d, %d, %d)',
      $user->uid, (int)$level_id, time()
    );
  }
  
  /**
   * Checks premission, whether the current user is allowed to access
   * the given level and the user is allowed to play the game
   * 
   * @param mixed $level
   *     The level (nr | name | NULL = current object)
   * @return
   *     Returns TRUE, if the current user is granted to access the given level.
   */
  public function has_access($level = NULL) {
    if( $this->has_game_access() ) {
      return $this->has_level_access($level);
    }
    return FALSE;
  }
  
  /**
   * Chechs the permission to play a specific level.
   * 
   * @param mixed $level
   * 		NULL, the current entity information is used.
   * 		int, the level will be loaded by number
   * 		string, the level will be loaded by name
   * 
   * @return
   * 		TRUE, if the current user is allowed to play the game.
   * 		FALSE, if the current user is not allowed to play the game.
   */
  public function has_level_access($level = NULL) {
    // check the current level
    $level_current = $this->get_level($level);
    if( !$level_current->is_entity() ) {
      return FALSE;
    }
    
    // A valid level is set.
    // Check, whether we are allowed to play the given level.
    // (Maybee we have to play some levels before!)
    $level_highest = self::get_highest_playable_level();
    if( !$level_highest->is_entity() ) {
      // We played all levels!
      return TRUE;
    }
    return $level_highest->get_entity()->get_level() >= $level_current->get_entity()->get_level();
  }
  
  /**
   * Returns, whether the current user is allowed to play a game or not.
   * 
   * @return
   * 		TRUE, if the current user is allowed to play the game.
   * 		FALSE, if the current user is not allowed to play the game.
   */
  public function has_game_access() {
    return user_access('play ethifygame');
  }
  
  /**
   * This method returns a ethifygame object with the highest playable level object.
   * 
   * @return
   *     Returns the highest level, a user is allowed to play.
   *     If all levels are played, is_entity() of the given
   *     object will return false.
   */
  public static function get_highest_playable_level() {
    global $user;
    $o = new ethifygame(
      db_fetch_array(
          db_query(
            'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
            ' FROM {ethifygame_levels} LEFT OUTER JOIN {ethifygame_played} ON (eg_level_id = eg_played_level_id AND eg_played_user_id = %d)' .
            ' WHERE eg_level_state = %d AND eg_played_level_id IS NULL' .
            ' ORDER BY eg_level_nr ASC',
            $user->uid, ETHIFYGAME_LEVEL_STATE_ACTIVE
    )));
    return $o;
  }
  
  /**
   * Fetches the given levels, played by the current user form the database
   * 
   * @param int $level_id
   *     The level_id or NULL for all levels
   * @return
   *     Returns an array ob objects containing user, level and datetime.
   */
  private function get_user_level_entries($level_id = NULL) {
    global $user;
    $result = NULL;
    if( $level_id == NULL ) {
      $result = db_query(
        'SELECT eg_played_user_id AS user_id, eg_played_level_id AS level_id, eg_played_activate_timestamp AS activate_timestamp' .
        ' FROM {ethifygame_played}' .
        ' WHERE eg_played_user_id = %d',
        (int)$user->uid
      );
    }
    else {
      $result = db_query(
        'SELECT eg_played_user_id AS user_id, eg_played_level_id AS level_id, eg_played_activate_timestamp AS activate_timestamp' .
        ' FROM {ethifygame_played}' .
        ' WHERE eg_played_user_id = %d AND eg_played_level_id = %d',
        (int)$user->uid, (int)$level_id
      );
    }
    
    // fetch data
    $ret_value = array();
    while( $row = db_fetch_object($result) ) {
      $ret_value[] = $row;
    }
    
    return $ret_value;
  }
  
  /**
   * Returns, wether the level is the first level in this game or not.
   * 
   * @param mixed $level
   *     Either the level-number or the level-name.
   * @return
   *     Returns TRUE, if this level is the first in the game.
   */
  public function is_first_level($level = NULL) {
    $level_current = $this->get_level($level);
    if( !$level_current->is_entity() ) {
      return FALSE;
    }
    
    $level_first = $this->get_first_level();
    return $level_first->get_entity()->get_id() == $level_current->get_entity()->get_id();
  }
  
  /**
   * Returns, wether the level is the last level in this game or not.
   * 
   * @param mixed $level
   *     Either the level-number or the level-name.
   * @return
   *     Returns TRUE, if this level is the first in the game.
   */
  public function is_last_level($level = NULL) {
    $level_current = $this->get_level($level);
    if( !$level_current->is_entity() ) {
      return FALSE;
    }
    
    $level_last = $this->get_last_level();
    return $level_first->get_entity()->get_id() == $level_current->get_entity()->get_id();
  }
  
  /**
   * Returns a ethifygame object with the specified level set.
   * 
   * @param mixed $level
   *     Either the level-number or the level-name.
   *     If level is not given or NULL, the current object will be returned.
   * @return
   *     Returns a ethifygame object by the given level.
   */
  public function get_level($level = NULL) {
    if( $level != NULL ) {
      if( is_int($level) ) {
        // level is an nr
        if( !$this->is_entity() || $level != $this->get_entity()->get_level() ) {
          return self::factory_get_level($level);
        }
      }
      else {
        // level is a text
        if( !$this->is_entity() || $level != $this->get_entity()->get_name() ) {
          return self::factory_get_level_by_name($level);
        }
      }
    }
    // if the given level is empty or matches the
    // current object, return the current object
    return $this;
  }
  
  /**
   * @return
   *     Returns an object of the first level.
   */
  public function get_first_level() {
    return self::factory_get_first_level();
  }
  
  /**
   * @return
   *     Returns an object of the last level.
   */
  public function get_last_level() {
    return self::factory_get_last_level();
  }
  
  /**
   * Creates a new instance of ethifygame and
   * loads the given level-nr into its entity
   * 
   * @param int $nr
   *     The level-nr, which should be loaded
   * @return 
   *     Returns the ethifygame instance with the given level
   */
  public static function factory_get_level($nr) {
    return new ethifygame(
        db_fetch_array(
            db_query(
                'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
                ' FROM {ethifygame_levels}' .
                ' WHERE eg_level_nr = %d AND eg_level_state = %d',
                $nr, ETHIFYGAME_LEVEL_STATE_ACTIVE
    )));
  }
  
  /**
   * Creates a new instance of ethifygame and
   * loads the given level by id into its entity
   * 
   * @param int $id
   *     The level-id, which should be loaded
   * @return
   *     Returns the ethifygame instance with the given level
   */
  public static function factory_get_level_by_id($id) {
    return new ethifygame(
        db_fetch_array(
            db_query(
                'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
                ' FROM {ethifygame_levels}' .
                " WHERE eg_level_id = %d",
                $id
    )));
  }
  
  /**
   * Creates a new instance of ethifygame and
   * loads the given level by name into its entity
   * 
   * @param string $name
   *     The level-name, which should be loaded
   * @return
   *     Returns the ethifygame instance with the given level
   */
  public static function factory_get_level_by_name($name) {
    return new ethifygame(
        db_fetch_array(
            db_query(
                'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
                ' FROM {ethifygame_levels}' .
                " WHERE eg_level_name = '%s'",
                $name
    )));
  }
  
  /**
   * Creates a new instance of ethifygame and
   * loads the first level into its entity
   * 
   * @return
   *     Returns the ethifygame instance with the first level
   */
  public static function factory_get_first_level() {
    if( self::$first_level === NULL ) {
      self::$first_level = new ethifygame(
          db_fetch_array(
              db_query(
                  'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
                  ' FROM {ethifygame_levels}' .
                  ' WHERE eg_level_state = %d ' .
                  ' ORDER BY eg_level_nr ASC',
                  ETHIFYGAME_LEVEL_STATE_ACTIVE
      )));
    }
    return self::$first_level;
  }
  
  /**
   * Creates a new instance of ethifygame and
   * loads the last level into its entity
   * 
   * @return
   *     Returns the ethifygame instance with the last level
   */
  public static function factroy_get_last_level() {
    if( self::$last_level === NULL ) {
      self::$last_level = new ethifygame(
          db_fetch_array(
              db_query(
                  'SELECT eg_level_id, eg_level_name, eg_level_entry_path, eg_level_nr, eg_level_state' .
                  ' FROM {ethifygame_levels}' .
                  ' WHERE eg_level_state = %d ' .
                  ' ORDER BY eg_level_nr DESC',
                  ETHIFYGAME_LEVEL_STATE_ACTIVE
      )));
    }
    return self::$last_level;
  }
  
  /**
   * Creates an empty ethifygame object.
   * 
   * @return
   *     Returns a empty ethifygame object.
   */
  public static function factory_get_empty() {
    return new ethifygame();
  }
  
  /**
   * Instantiates a new ethifygame object
   * 
   * @param array $level_data
   *     NULL or a database row of the game level
   */
  public function __construct($level_data = NULL) {
    if( $level_data && is_array($level_data) ) {
      $this->entity = new ethifygame_level($level_data);
    }
  }
  
  
  /**
   * @return
   *     Returns the next level of the current game.
   *     If the current object is invalid or the last level,
   *     an empty object is returned.
   */
  public function get_next_level() {
    if( $this->is_entity() ) {
      return self::factory_get_level($this->get_entity()->get_level() + 1);
    }
    return self::factory_get_empty();
  }
  
  /**
   * @return
   *     Returns the previous level of the current game.
   *     If the current object is invalid or the first level,
   *     an empty object is returned.
   */
  public function get_previous_level() {
    if( $this->is_entity() ) {
      return self::factory_get_level($this->get_entity()->get_level() - 1);
    }
    return self::factory_get_empty();
  }
  
  /**
   * @return
   *     Returns TRUE, if the entity in this object exists in the database, otherwise FALSE.
   */
  public function is_entity() {
    return $this->entity !== NULL && $this->get_entity()->get_id() !== NULL;
  }
  
  /**
   * @return
   *     Returns the current entity, loaded in this object.
   */
  public function get_entity() {
    if( $this->entity === NULL ) {
      $this->entity = new ethifygame_level();
    }
    return $this->entity;
  }
  
  /**
   * @return
   *     Returns the current entity as reference.
   */
  public function &get_entity_ref() {
    if( $this->entity === NULL ) {
      $this->entity = new ethifygame_level();
    }
    return $this->entity;
  }
  
  /**
   * Redirects the user to another level.
   * 
   * @param mixed $level
   *     NULL or level-nr or level name
   */
  function level_goto($level = NULL) {
    $level_current = $this->get_level($level);
    
    if( $level_current->is_entity() ) {
      drupal_goto($level_current->get_entity()->get_start_path());
    }
    else {
      // If level is invalid, drupal redirects to home-page.
      drupal_set_message('EthifyGame: Redirect-URL not found');
      drupal_goto('ethifygame/home');
    }
  }
  
  /**
   * Redirects the user to the next level.
   * 
   * @param mixed $level
   *     NULL or level-nr or level name
   */
  function goto_next($level = NULL) {
    $level_current = $this->get_level($level);
    if ($level_current->is_entity()) {
      $level_next = $level_current->get_next_level();
      if ($level_next->is_entity() && $level_next->has_access()) {
        // goto next level
        $level_next->level_goto();
      }
    }
    // no valid object or permission
    // let ethifygame decide, where to jump
    ethifygame_goto();
  }
  
  /**
   * Redirects the user to the previous level.
   * 
   * @param mixed $level
   *     NULL or level-nr or level name
   */
  function goto_prev($level = NULL) {
    $level_current = $this->get_level($level);
    if ($level_current->is_entity()) {
      $level_prev = $level_current->get_previous_level();
      if ($level_prev->is_entity() && $level_prev->has_access()) {
        // goto next level
        $level_prev->level_goto();
      }
    }
    // no valid object or permission
    // as we are moving previously, jump to home page instead of last level
    ethifygame_goto_home();
  }
}

class ethifygame_level {
  private $id = NULL;
  private $name = NULL;
  private $start_path = NULL;
  private $level = NULL;
  private $state = NULL;
  
  /**
   * @param array $level_data
   *     array(id, name, start_path, level)
   */
  public function __construct($level_data = NULL) {
    if( $level_data != NULL && is_array($level_data) && count($level_data) == 5 ) {
      $this->id = $level_data['eg_level_id'];
      $this->name = $level_data['eg_level_name'];
      $this->start_path = $level_data['eg_level_entry_path'];
      $this->level = $level_data['eg_level_nr'];
      $this->state = $level_data['eg_level_state'];
    }
  }
  
  public function get_id() {
    return $this->id;
  }
  public function get_name() {
    return $this->name;
  }
  public function get_start_path() {
    return $this->start_path;
  }
  public function get_level() {
    return $this->level;
  }
  public function get_state() {
    return $this->state;
  }
  
  public function set_id($id) {
    if( $id === NULL || (int)$id < 0 ) {
      $id = NULL;
    }
    else {
      $this->id = (int)$id;
    }
  }
  public function set_name($name) {
    if( $name === NULL ) {
      $this->name = NULL;
    }
    else {
      $this->name = trim((string)$name);
    }
  }
  public function set_start_path($start_path) {
    if( $start_path === NULL ) {
      $this->start_path = NULL;
    }
    else {
      $this->start_path = trim((string)$start_path);
    }
  }
  public function set_level($level) {
    if( $level === NULL || (int)$level < 0 ) {
      $this->level = NULL;
    }
    else {
      $this->level = (int)$level;
    }
  }
  public function set_state($state) {
    if( $state == ETHIFYGAME_LEVEL_STATE_ACTIVE ) {
      $this->state = ETHIFYGAME_LEVEL_STATE_ACTIVE;
    }
    else if( $state == ETHIFYGAME_LEVEL_STATE_INACTIVE ) {
      $this->state = ETHIFYGAME_LEVEL_STATE_INACTIVE;
    }
    else {
      $this->state = ETHIFYGAME_LEVEL_STATE_UNKNOWN;
    }
  }
}
