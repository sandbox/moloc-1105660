<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the ethifygame module.
 */

/**
 * Builds the form for configuring the settings
 * of the ethifygame module.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function _ethifygame_admin_settings($form_settings) {
  $home_path = drupal_get_normal_path('ethifygame/home');
  if ($home_path == 'ethifygame/home') {
    $home_path = 'asfd';
  }
  $finish_path = drupal_get_normal_path('ethifygame/finish');
  if ($finish_path == 'ethifygame/finish') {
    $finish_path = '';
  }

  $form = array();
  $form['table_levels'] = array(
    '#type' => 'markup',
    '#value' => _ethifygame_admin_settings_render(),
  );
  $form['eg_home_page'] = array(
    '#title' => 'Start page',
    '#description' => 'Path to the startpage, explaining the game. If you do not need a start page, use <i>ethifygame/level/1</i>. You can use only internal pathes (eg. nodes/pages).',
    '#type' => 'textfield',
    '#default_value' => $home_path,
  );
  $form['eg_finish_page'] = array(
    '#title' => 'End page',
    '#description' => 'Path to the end page. This page is shown, when the user played all levels. You can use only internal pathes (eg. nodes/pages).',
    '#type' => 'textfield',
    '#default_value' => $finish_path,
  );
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'] = array();
  $form['#submit'][] = '_ethifygame_admin_settings_form_submit';
  $form['#theme'] = 'system_settings_form';
  
  return $form;
}

function _ethifygame_admin_settings_form_submit($form, &$form_state) {
  // is there a nice solution for this workaround?
  // as you usually only set this form on install time, there will be no performance problem
  path_set_alias(NULL, 'ethifygame/home');
  path_set_alias(NULL, 'ethifygame/finish');
  // set the path
  path_set_alias($form_state['values']['eg_home_page'], 'ethifygame/home');
  path_set_alias($form_state['values']['eg_finish_page'], 'ethifygame/finish');

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Moves a level up or down in the level hirarchy of the game.
 * @param int $level_id
 * 		The level, which should be reordered.
 * @param string $direction
 * 		inc: increase the level-number by one (n+1 will decrease)
 * 		dec: decrease the level-number by one (n-1 will increase)
 */
function _ethifygame_admin_settings_level_reorder($level_id, $direction) {
  // validate input
  if (is_numeric($level_id) && in_array($direction, array('inc', 'dec'))) {
    $level = ethifygame::factory_get_level_by_id(intval($level_id));
    if ($level->is_entity()) {
      // level exist
      if ($direction == 'inc') {
        $level->get_entity_ref()->set_level($level->get_entity()->get_level() + 1);
      }
      else {
        $level->get_entity_ref()->set_level($level->get_entity()->get_level() - 1);
      }
      $level->save();
    }
  }
  // redirect back to the admin page
  drupal_goto('admin/settings/ethifygame');
}

function _ethifygame_admin_settings_render() {
  $levels = ethifygame_get_levels();

  $content = _ethifygame_render_level_header();
  if( !empty($levels) ) {
    foreach ($levels as $level) {
      $content .= _ethifygame_render_level($level);
    }
  } else {
    $content .= _ethifygame_render_level_none();
  }
  $content .= _ethifygame_render_level_footer();

  return $content;
}

function _ethifygame_render_level_header() {
  $output = '';
  $output .= '<table class="sticky-enabled">';
  $output .= "\r\n\t<thead>";
  $output .= "\r\n\t\t<tr>";
  $output .= "\r\n\t\t\t" . '<th class="active">Level Name</th>';
  $output .= "\r\n\t\t<th>Start Path</th>";
  $output .= "\r\n\t\t<th>Level</th>";
  $output .= "\r\n\t\t<th>Active</th>";
  $output .= "\r\n\t\t<th>Move</th>";
  $output .= "\r\n\t\t</tr>";
  $output .= "\r\n\t</thead>";
  $output .= "\r\n\t<tbody>";

  return $output;
}

function _ethifygame_render_level_footer() {
  $output = '';
  $output .= "\r\n\t</tbody>";
  $output .= "\r\n</table>";

  return $output;
}

function _ethifygame_render_level($level) {
  static $odd = TRUE;

  $output = '';
  if( $level ) {
    $output .= "\r\n\t\t" . '<tr class="';
    if( $odd ) {
      $output .= 'odd';
    } else {
      $output .= 'even';
    }
    $odd = !$odd;
    $output .= '"><td>';
    $output .= check_plain($level->get_name());
    $output .= '</td><td>';
    $output .= check_plain($level->get_start_path());
    $output .= '</td><td>';
    $output .= $level->get_level();
    $output .= '</td><td>';
    if( $level->get_state() ) {
      $output .= '<span style="color:green;">Enabled</span>';
    } else {
      $output .= '<span style="color:red;">Disabled</span>';
    }
    $output .= '</td><td>';
    $output .= '<a href="';
    $output .= url('admin/settings/ethifygame/move/'. $level->get_id() .'/dec', array('absolute' => TRUE));
    $output .= '" style="padding: 0 5px; margin: 0 5px;">Up</a>';
    $output .= '<a href="';
    $output .= url('admin/settings/ethifygame/move/'. $level->get_id() .'/inc', array('absolute' => TRUE));
    $output .= '" style="padding: 0 5px; margin: 0 5px;">Down</a>';

    $output .= '</td></tr>';
  }

  return $output;
}

function _ethifygame_render_level_none() {
  $output = '';
  $output .= "\r\n\t\t" . '<tr class="odd><td class="active" colspan="4">There are no levels installed</td></tr>';
  return $output;
}


//function ethifygame_admin_settings() {
//  $options = node_get_types('names');
//
//    $form['ethifygame_node_types'] = array(
//    '#type' => 'checkboxes',
//    '#title' => t('Content types with attached Ethify Game:'),
//    '#options' => $options,
//    '#default_value' => variable_get('ethifygame_node_types', array('page')),
//    '#description' => t('The Ethify Game be offered for the selected content types.'),
//  );
//
//  return system_settings_form($form);
//}